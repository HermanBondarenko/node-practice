import {promisify} from 'util';
import * as utils from 'test-utils';

const params = {password: 'sdf234fdsf32cdsc'};

const promisifyRunMePlease = promisify(utils.runMePlease);

(async () => {
    const result = await promisifyRunMePlease(params).catch(console.log);

    console.log(result);
})();
